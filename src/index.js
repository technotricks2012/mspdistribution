const utils = require("./utils");
const config = require("../config.json");

const iOSPublicLink = config.iOSPublicLink;
const iOSVersion = config.iOSVersion;



const emails = [];

function main(folder, file) {
  const filePath = folder + "/" + file;

  console.log("\x1b[32m%s\x1b[0m",filePath);

 utils.clearArray(emails);

  utils
    .extractExcel(filePath)
    .on("data", (data) => {
        const email = data["Email"];
        emails.push(email)
    })
    .on("end", () => {
        console.log("\x1b[32m%s\x1b[0m", emails.join());
       utils.sendMail(emails.join())

    });
}


main("Files", "iOSUsers.csv");
