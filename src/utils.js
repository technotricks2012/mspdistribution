const csv = require("csv-parser");
const fs = require("fs");
const config = require("../config.json");

const iOSPublicLink = config.iOSPublicLink;
const iOSVersion = config.iOSVersion;

var nodemailer = require('nodemailer');


module.exports = {
  extractExcel(filePath) {
    return fs.createReadStream(filePath).pipe(
      csv({
        skipLines: 1,
        headers: ["First Name", "Last Name", "Email"],
      })
    );
  },

  clearArray(array) {
    while (array.length) {
      array.pop();
    }
  },

  sendMail(recipientEmails){
      const email = "technotricks2012@gmail.com"
      const password = "kishore12313"
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: email,
          pass: password
        }
      });

      var mailOptions = {
        from: email,
        to: recipientEmails,
        subject: "MobilitySensing "+ iOSVersion +" for iOS is ready to test",
        html: generateHtml("Test Kishore")
      };

      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
        
  },




};


function generateHtml(data){
    var htmlSTring = 
   
    `
    <div class="es-wrapper-color">
<table class="es-wrapper" style="height: 587px;" width="602" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-email-paddings st-br" style="width: 604px;" valign="top">
<table class="es-header esd-header-popover" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="esd-stripe esd-checked" style="background-image: url('https://hpy.stripocdn.email/content/guids/CABINET_d21e6d1c5a6807d34e1eb6c59a588198/images/20841560930387653.jpg'); background-color: transparent; background-position: center bottom; background-repeat: no-repeat;" align="center" bgcolor="transparent">
<div>
<table class="es-header-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center" bgcolor="transparent">
<tbody>
<tr>
<td class="esd-structure es-p20t es-p20r es-p20l" align="left">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-container-frame" align="center" valign="top" width="560">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-block-image" style="font-size: 0px;" align="center"><atarget="_blank" rel="noopener"><img style="display: block; width: 126px; height: 117px;" src="resources/icon.png" alt="" width="126" /></a></td>
</tr>
<tr>
<td class="esd-block-spacer" align="center" height="65">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<table class="es-content" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="esd-stripe" style="background-color: transparent;" align="center" bgcolor="transparent">
<table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center" bgcolor="transparent">
<tbody>
<tr>
<td class="esd-structure es-p10t es-p10b es-p20r es-p20l" style="background-position: left bottom;" align="left">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-container-frame" align="center" valign="top" width="560">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="esd-structure es-p30t es-p15b es-p30r es-p30l" style="border-radius: 10px 10px 0px 0px; background-color: #ffffff; background-position: left bottom;" align="left" bgcolor="#ffffff">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-container-frame" align="center" valign="top" width="540">
<table style="background-position: left bottom;" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-block-text" align="center">
<h1>MobilitySensing</h1>
<p><span style="color: #808080;">for iOS</span></p>
</td>
</tr>
<tr>
<td class="esd-block-text es-p20t" align="center">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="esd-structure es-p5t es-p5b es-p30r es-p30l" style="border-radius: 0px 0px 10px 10px; background-position: left top; background-color: #ffffff;" align="left">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-container-frame" align="center" valign="top" width="540">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-block-text" align="left">
<h1 style="text-align: center; margin-top: 20px;"><a href="https://testflight.apple.com/join/W4mNGwFN" target="_blank" rel="noopener"><span style="background-color: #000080; color: #ffffff;"><strong>Download</strong></span></a></h1>
<ul>
<li>Lorem Ipsum is simply dummy text&nbsp;</li>
</ul>
</td>
</tr>
<tr>
<td class="esd-block-text" align="left">&nbsp;</td>
</tr>
<tr>
<td class="esd-block-text" align="left">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="esd-structure es-p30t es-p30b es-p30r es-p30l" style="background-position: left bottom;" align="left">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-container-frame" style="text-align: justify;" align="center" valign="top" width="540"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>`
  
return htmlSTring
}